import { slideToggle } from '../../base/js/core'

const burger = document.querySelector('.s-header__burger')
const mobileMenu = document.getElementById('mobile-menu')
burger.addEventListener('click', () => {
	burger.classList.toggle('is-active')
	slideToggle(mobileMenu)
})
